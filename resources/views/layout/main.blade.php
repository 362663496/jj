<?php
$user = \Illuminate\Support\Facades\Auth::user();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- jQuery 2.1.4 -->
    <script src="js/jquery-3.4.1.min.js"></script>

    <script src="js/app.js"></script>
    <script src="js/common.js"></script>
    <!-- http://www.fontawesome.com.cn/icons-ui/ ICON-->
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="font-awesome/css/font-awesome.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- SlimScroll -->
    <script src="js/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="js/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.js"></script>

    <!-- Vue
    <script src="js/vue.js"></script>
    -->

        <script src="layui/layui.all.js"></script>
        <script src="layui/layui.js"></script>
        <link rel="stylesheet" href="layui/css/layui.css">

        <!-- ag-grid-->
    <script src="ag-grid-community/ag-grid-community.js"></script>
    <link rel="stylesheet" href="ag-grid-community/styles/ag-grid.min.css">
    <link rel="stylesheet" href="css/ag-theme-zzy.min.css">
    <link rel="stylesheet" href="css/ag-theme-alpine.css">

    <script src="http://cdn.bootcss.com/bootstrap/2.3.1/js/bootstrap-alert.min.js"></script>
    <link rel="stylesheet" href="css/app.css">
    <style>
        .modal.left .modal-dialog,.modal.right .modal-dialog{position:fixed;margin:auto;width:320px;height:100%;-webkit-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);-o-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}
        .modal.left .modal-content,.modal.right .modal-content{height:100%;overflow-y:auto}
        .modal.left .modal-body,.modal.right .modal-body{padding:15px 15px 80px}
        .modal.left.fade .modal-dialog{left:-320px;-webkit-transition:opacity .3s linear,left .3s ease-out;-moz-transition:opacity .3s linear,left .3s ease-out;-o-transition:opacity .3s linear,left .3s ease-out;transition:opacity .3s linear,left .3s ease-out}
        .modal.left.fade.in .modal-dialog{left:0}
        .modal.right.fade .modal-dialog{right:-320px;-webkit-transition:opacity .3s linear,right .3s ease-out;-moz-transition:opacity .3s linear,right .3s ease-out;-o-transition:opacity .3s linear,right .3s ease-out;transition:opacity .3s linear,right .3s ease-out}
        .modal.right.fade.in .modal-dialog{right:0}
        .modal-content{border-radius:0;border:none}
        .modal-header{border-bottom-color:#eee;background-color:#fafafa}

        @media screen and (min-width: 320px) and (max-width: 480px) {
            #alert {
                top: 19%;
            }
        }

    </style>
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-blue fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>m</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Admin</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
{{--                    <li class="dropdown messages-menu">--}}
{{--                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
{{--                            <i class="fa fa-envelope-o"></i>--}}
{{--                            <span class="label label-success">10</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?=$user->name?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                <p>
                                    <?=$user->name?> - Web Developer
                                </p>
                                <p><small>不塞不流，不止不行</small></p>
                            </li>
                            <!-- Menu Body -->
{{--                            <li class="user-body">--}}
{{--                                <div class="col-xs-4 text-center">--}}
{{--                                    <a href="#">Followers</a>--}}
{{--                                </div>--}}
{{--                                <div class="col-xs-4 text-center">--}}
{{--                                    <a href="#">Sales</a>--}}
{{--                                </div>--}}
{{--                                <div class="col-xs-4 text-center">--}}
{{--                                    <a href="#">Friends</a>--}}
{{--                                </div>--}}
{{--                            </li>--}}
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="{{route('logout')}}" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                </ul>
            </div>
        </nav>
    </header>
    <!-- =============================================== -->
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?=$user->name?></p>
                    <p>Welcome！</p>
                </div>
            </div>
            <!-- search form -->
            <!--    搜索
                <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                        class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>
            -->
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li id="index">
                    <a href="/">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-files-o"></i>
                        <span>Layout Options</span>
                        <span class="fa fa-angle-left pull-right"></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Boxed</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Fixed</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
                    </ul>
                </li>
                <li id="userList">
                    <a href="/goodsList">
                        <i class="fa fa-th"></i> <span>Widgets</span>
                    </a>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
       @yield('content')
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right">
            <b>CHANG JIANG</b>
        </div>
        <strong>JINGJING</strong>
    </footer>
</div><!-- ./wrapper -->

<!-- loading -->
<div class="modal fade" id="loading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop='static'>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">提示</h4>
            </div>
            <div class="modal-body">
                请稍候。。。<span id="result"></span>
            </div>
        </div>
    </div>
</div>

</body>
</html>
@yield('script')

<script>
    $(function (){
        checkActiveNav();
    })
    function checkActiveNav() {
        //判断当前访问的是哪个标签页加状态
        var pathname = window.location.pathname;
        if (pathname == '/') {
            pathname = 'index';
        } else {
            pathname = pathname.substring(1);
        }
        $('#'+ pathname).addClass('active');
    }
</script>
