@extends('layout.main')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>商品列表</h1>
    </section>
    <!-- Main content -->
    <section class="content" style="margin-top: 30px">
        <div id="myGrid" style="height:50em;" class="ag-theme-zzy"></div>
    </section>

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="myModal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" style="margin-top: 15px" id="info_form">
                        <div class="form-group">

                            <label for="chemistry_name" class="col-sm-2 control-label">化学名字</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="chemistry_name" id="chemistry_name" placeholder="化学名字">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product_name" class="col-sm-2 control-label">商品名字</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="product_name" id="product_name" placeholder="商品名字">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="main_cure" class="col-sm-2 control-label">主要治疗</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="main_cure" id="main_cure" placeholder="主要治疗">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="manufacture_date" class="col-sm-2 control-label">生产日期</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="manufacture_date" id="manufacture_date">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="expires" class="col-sm-2 control-label">过期时间</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="expires" id="expires" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="note" class="col-sm-2 control-label">注意事项</label>
                            <div class="col-sm-10">
                                <label>
                                    <textarea class="form-control" rows="3" name="note" id="note"></textarea>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="num" class="col-sm-2 control-label">数量</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="num" id="num" placeholder="数量" oninput="value = onlyInputIntval(value)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label">价格</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="price" id="price" placeholder="价格">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="quality" class="col-sm-2 control-label">保质期</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="quality" id="quality" placeholder="保质期">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="stock" class="col-sm-2 control-label">库存</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="stock" id="stock" placeholder="库存"  oninput="value = onlyInputIntval(value)">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('script')
    <script>
        var columnDefs = [
            {
                headerName: "操作",
                field: "",
                width:150,
                cellRenderer: function (params) {
                    var html = `
                                <button class="btn btn-danger"  onclick="deleteRow(${params.data.id})">删除</button>
                                <button class="btn btn-warning" onclick="updateRow(${params.data.id})">修改</button>
                            `;
                    return html;
                }
            },
            {headerName: "化学名字", field: "chemistry_name",width:100},
            {headerName: "商品名字", field: "product_name",width:100},
            {headerName: "主要治疗", field: "main_cure",width:100},
            {headerName: "注意事项", field: "note",width:200},
            {headerName: "数量", field: "num",width:70},
            {headerName: "价格", field: "price",width:70},
            {headerName: "照片", field: "photo",width:100, cellRenderer: function (params) {
                    return `<img src="${params.data.photo}" class="img-md" alt="photo" onclick="previewImg(this)">`;
                }},
            {headerName: "保质期天数", field: "quality",width:70},
            {headerName: "生产日期", field: "manufacture_date",width:120},
            {headerName: "过期时间", field: "expires",width:120},
            {headerName: "库存", field: "stock",width:70},
        ];


        // let the grid know which columns and what data to use

         gridOptions = {
             columnDefs: columnDefs,
             rowData: [],
             defaultColDef: {
                sortable: true,
                sortingOrder: ['desc' ,'asc' ,null],
                cellClass: 'text-center',
            },
            animateRows: true,
            multiSortKey: 'ctrl',
            onGridReady: function(params) {
              //  params.api.sizeColumnsToFit();//调整表格大小自适应
            },
            getRowNodeId: function (data) {
                return data.id;
            }
        };
        // setup the grid after the page has finished loading
        $(function(){
            getList();
            layui.use('laydate', function() {
                var laydate = layui.laydate;
                //日期选择器
                laydate.render({
                    elem: '#manufacture_date'
                    //,type: 'date' //默认，可不填
                });
                laydate.render({
                    elem: '#expires'
                    //,type: 'date' //默认，可不填
                });
            });
        });

        //删除
        function deleteRow(id) {
            layer.alert('墨绿风格，点击确认看深蓝', {
                    skin: 'layui-layer-molv' //样式类名
                    ,closeBtn: 0
            });
            $().app_alert('错误！', "danger");
            //alert(id);
        }

        //修改
        function updateRow(id) {
            let row = gridOptions.api.getRowNode(id);
            if (row && row.data) {
                $('#info_form').fillData({
                    chemistry_name      : row.data.chemistry_name,
                    product_name        : row.data.product_name,
                    main_cure           : row.data.main_cure,
                    note                : row.data.note,
                    num                 : row.data.num,
                    price               : row.data.price,
                    photo               : row.data.photo,
                    quality             : row.data.quality,
                    manufacture_date    : row.data.manufacture_date,
                    expires             : row.data.expires,
                    stock               : row.data.stock,
                });
            }
            $('#myModal').modal('show')
        }

        function getList() {
            //使用ajax发送请求
            $('#loading').modal('show');
            $.ajax({
                url:"/goodsList",
                type:"post",
                dataType:"json",
                success:function(res){
                    if (res.code == 200) {
                        if (gridOptions.rowData.length) {
                            gridOptions.api.setRowData(res.data);
                        } else {
                            gridOptions.rowData = res.data;
                            var gridDiv = document.querySelector('#myGrid');
                            new agGrid.Grid(gridDiv, gridOptions);
                        }
                    } else {
                        $().app_alert(res.msg);
                    }
                    $('#loading').modal('hide');
                },
                error: function (res) {
                    $('#loading').modal('hide');
                }
            });
        }
    </script>
@endsection

