<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/test', [\App\Http\Controllers\TestController::class, 'test']);
Route::any('/register', [\App\Http\Controllers\TestController::class, 'register']); //注册账号。

Route::any('/login', [\App\Http\Controllers\LoginController::class, 'login'])->name('login');
Route::any('/logout', [\App\Http\Controllers\LoginController::class, 'logout'])->name('logout');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', [\App\Http\Controllers\AdminController::class, 'index']);
    Route::get('/goodsList', [\App\Http\Controllers\AdminController::class, 'goodsList']);
    Route::post('/goodsList', [\App\Http\Controllers\AdminController::class, 'goodsList']);
});

//微信公众号
Route::any('/wechat', 'WeChatController@serve');

//微信授权
Route::group(['middleware' => ['web', 'wechat.oauth'], 'prefix' => 'wechat'], function () {
    Route::get('/user', function () {
        $user = session('wechat.oauth_user.default'); // 拿到授权用户资料
        dd($user);
    });
});

