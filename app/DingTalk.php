<?php


namespace App;


use GuzzleHttp\Client;

class DingTalk
{
    //晶晶群机器人token
    const JING_DING_TOKEN = 'd274d9f02ca06931c160d30a34b65d6e3933c49b9876e01b361743c8acae27a6';

    //请求地址
    private $send_url = 'https://oapi.dingtalk.com/robot/send';

    //token变量
    private $token;

    //消息数据
    private $message_data;

    //关键词
    private $keywords = '_';

    /**
     * DingTalk constructor.
     * @param string $token
     */
    public function __construct(string $token = self::JING_DING_TOKEN) {
        $this->token = $token;
        return $this;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function textMessage(string $message, array $at = [], bool $atAll = false) {
        $data = [
            'msgtype' => 'text',
            'text' => [
                'content' => $message.$this->keywords
            ],
            'at' => [
                "atMobiles" => $at,
                "isAtAll" => $atAll,
            ],
        ];
        $this->message_data = $data;
        return $this;
    }

    public function send() {
        $client = new Client(['base_uri' => $this->send_url]);
        $res = $client->request('POST', '', [
            'json' => $this->message_data,
            'headers' => [
                'Content-type' => 'application/json',
                "Accept" => "application/json"
            ],
            'query' => [
                'access_token' => $this->token,
            ]
        ]);
        $data = $res->getBody()->getContents();
        return $data;
    }
}
