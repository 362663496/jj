<?php


namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class TestController extends Controller
{
    private $a = 123;

    public function test() {
        $date1 = Carbon::parse('2020-07-22')->diff(Carbon::now())->format('%y年%M月%D天%H小时%I分钟%S秒');    //Laravel 框架 Carbon扩展
        $date2 = date('Y-m-d H:i:s');        //系统
        dd($date1, $date2);
    }

    public function register(Request  $request) {
        $email = $request->get('email');
        $password = $request->get('password');
        $user = new User();
        $user->email = $email;
        $user->password = Hash::make($password);
        $user->save();
    }
}
