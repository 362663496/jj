<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function success($data, $msg = 'SUCCESS') {
        return json_encode(['code' => 200, 'data' => $data, 'msg' => $msg]);
    }

    protected function fail($msg = 'FAIL', $data = []) {
        return json_encode(['code' => 500, 'data' => $data, 'msg' => $msg]);
    }
}
