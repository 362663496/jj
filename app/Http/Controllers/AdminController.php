<?php


namespace App\Http\Controllers;

use App\Models\PesticideGoods;
use Illuminate\Http\Request;

class AdminController extends controller
{
    public function index() {
        return view('index');
    }

    public function goodsList(Request $request) {
        if ($request->isMethod('get')) {
            return view('goodsList');
        } else {
            $data = PesticideGoods::all();
            return $this->success($data);
        }
    }
}
