<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController
{
    //登录
    public function login(Request $request)
    {
        if ($request->isMethod('get')) {
            if (Auth::check()) {
                return redirect('/');
            }
            return view('login');
        }
        return $this->authenticate($request);
    }

    //退出登录
    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }


    /**
     * 处理认证尝试
     *
     * @param \Illuminate\Http\Request $request
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->intended('/');
        } else {
            return redirect('/login')->with('message', '账号或密码错误！');
        }
    }
}
