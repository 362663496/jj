!function($){
    //form表单回填数据
    $.fn.extend({
        fillData: function(obj){
            for (let i in obj) {
                $(this).find("input[name="+i+"]").val(obj[i]);
                $(this).find("textarea[name="+i+"]").text(obj[i]);
            }
        },
        app_alert: function(msg, type ='info'){

            let c = 'alert-info';
            if (type == 'warning') {
                c = 'alert-warning';
            } else if (type == 'error') {
                c = 'alert-error';
            } else if (type == 'danger') {
                c = 'alert-danger';
            } else if (type == 'success') {
                c = 'alert-success';
            }

            let html  = '<!--页面动画 -->\n' +
                '<!-- 弹框 -->\n' +
                '<div class="alert '+c+'" role="alert" id="alert">\n' +
                '    <button class="close" type="button" onclick="$(this).parent().hide()">&times;</button>\n' +
                '    <p>'+msg+'</p>\n' +
                '</div>';
            let el = $("body").append(html).find('#alert');
            el.show(500);
            app_alert_t = setTimeout(function() {
                if (el.css('display') != 'none') {
                    el.fadeOut(1500);
                    clearTimeout(app_alert_t);
                }
            }, 2000)
        },
    })
}(jQuery)

function isMobile() {
    var system ={};
    var p = navigator.platform;
    system.win = p.indexOf("Win") == 0;
    system.mac = p.indexOf("Mac") == 0;
    system.x11 = (p == "X11") || (p.indexOf("Linux") == 0);
    if(system.win||system.mac||system.xll){//如果是电脑跳转到百度
        return false;
    }
    return true;
}



